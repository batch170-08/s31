const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 3000;

// load the package that is inside the routes.js file in the repo
const taskRoute = require("./routes/routes.js")

app.use(express.json()); 
app.use(express.urlencoded({extended: true}));


mongoose.connect("mongodb+srv://aelacosta:Huskar0!1@wdc028-course-booking.fx063.mongodb.net/b170-to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));

db.once("open",() => console.log("We're connected to the database"))


// gives the app an access to the routes needed
// BASED URI ONCE WE WANT TO ACCESS THE ROUTES UNDER THE TASKS
app.use("/tasks", taskRoute)


app.listen(port,() => console.log(`Server is running at port ${port}`));