// containing the functions/statements/logic to be executed once a route has been triggered/entered
// responsible for execution of CRUD operations/methods.

// to give acces to the contents the for tasks.js file in the models folder; meaning it can use the Task model
const Task = require("../models/task.js");

//  QUERYING A TASK
// create controller functions that responds to the routes
module.exports.getAllTasks = () => {
    return Task.find({}).then(result =>{
        return result
    })
}

// CREATING A TASK

module.exports.createTask = (requestBody) => {
    let newTask = new Task({
        name: requestBody.name
})
     // saving mechanisms
 /*    then accepts 2 parameters
    -first parameter stores the result object; if we have successfully saved the object

    -second parameter stores the error object, should there be one*/

    return newTask.save().then((savedTask, error) => {
    if(error){
        console.log(error);
        return false
    }else{
        return savedTask
    }
})
}


// DELETING A TASK
/* 

1. look for the task with the corresponding id provided in the URL
2. delete the task
*/

module.exports.deleteTask = (taskId) => {
     // findByldAndRemove - finds the item to be deleted and removes it from the database; it uses id in looking for the document
    return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
        if (error) {
            console.log(error);
            return false
        } else {
            return removedTask
        }
    })
}


module.exports.updateTask = (taskId, requestBody) => {
    return Task.findById(taskId).then((result, error) => {
        if (error) {
            console.log(error)
            return false
        } else {
            result.name = requestBody.name;
            return result.save().then((updateTask, error) =>{
                if (error) {
                    console.log(error)
                    return false
                } else {
                    return updateTask
                }
            })
        }
    })
}


// ACTIVITY

module.exports.getTask = (taskId) => {
    return Task.findById(taskId).then((result,error) =>{
        if (error) {
            console.log(error)
            return false
        } else {
            return result
        }
    })
}

module.exports.updateTaskStatus = (taskId, requestBody) => {
    return Task.findById(taskId).then((result, error) => {
        if (error) {
            console.log(error)
            return false
        } else {
            result.status = requestBody.status;
            return result.save().then((updateTask, error) =>{
                if (error) {
                    console.log(error)
                    return false
                } else {
                    return updateTask
                }
            })
        }
    })
}
